<?php
    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 11/13/12, 9:24 AM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2012
     *
     */

    namespace Zippopotamus;

    /**
     * Api for interacting with Zippopotam.us for zip to country retreval
     */
    class API
        {
        /**
         * Sets Country code for requests
         *
         * @var string
         */
            public $country = 'us';
        /**
         * if set to true response will be json encoded for ajax consumption
         *
         * @var bool
         */
            public $ajax = false;

        /**
         * dummy holder for request info
         *
         * @var
         */
            private $request;

        /**
         * Returns location information based on provided zipcode
         *
         * @param $zipcode
         *
         * @return array|mixed
         */
            public function zipToCity($zipcode)
                {
                    $this->request = "/{$this->country}/{$zipcode}";
                    return $this->sendRequest();
                }

        /**
         * returns zipcode based on City and State
         *
         * @param $stateabbr
         * @param $city
         *
         * @return array|mixed
         */
            public function cityToZip($stateabbr, $city)
                {
                    $this->request = "/{$this->country}/{$stateabbr}/{$city}";
                    return $this->sendRequest();
                }

        /**
         * returns nearby locations
         *
         * @param $zipcode
         *
         * @return array|mixed
         */
            public function nearZip($zipcode)
                {
                    $this->request = "/nearby/{$this->country}/{$zipcode}";
                    return $this->sendRequest();
                }

        /**
         * Sends the request to the api
         *
         * @return array|mixed
         */
            private function sendRequest()
                {
                    $ch = curl_init('http://api.zippopotam.us/' . $this->request);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $reply = curl_exec($ch);
                    curl_close($ch);

                    $reply = json_decode($reply, true);

                    if ($this->ajax) {
                        return json_encode($reply);
                    }
                    return $reply;
                }
        }