<?php
    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 11/13/12, 9:40 AM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2012
     *
     */

    require_once (dirname(dirname(__FILE__)) . '/zippopotamus.php');

    use Zippopotamus\API;

    $zip = new API();
    $zip->ajax = true;

    switch ($_POST['action']) {
        default:
        case 'ziptocity':
            echo $zip->zipToCity($_POST['zipcode']);
            break;

        case 'citytozip':
            echo $zip->cityToZip($_POST['state'], $_POST['city']);
            break;

        case 'nearby':
            echo $zip->nearZip($_POST['zipcode']);
            break;
    }